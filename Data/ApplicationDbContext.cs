using AspNetMvc.Models;
using Blueshift.EntityFrameworkCore.MongoDB.Annotations;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;

namespace AspNetMvc.Data
{
    [MongoDatabase("AspNetMvc")]
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Dog> Dogs { get; set; }
        public DbSet<Owner> Owners { get; set; }

        public ApplicationDbContext() : this(new DbContextOptions<ApplicationDbContext>()) { }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
        //     var connectionString = "mongodb://localhost";
        //     //optionsBuilder.UseMongoDb(connectionString);

        //     var mongoUrl = new MongoUrl(connectionString);
        //     //optionsBuilder.UseMongoDb(mongoUrl);

        //     MongoClientSettings settings = MongoClientSettings.FromUrl(mongoUrl);
        //     //settings.SslSettings = new SslSettings
        //     //{
        //     //    EnabledSslProtocols = System.Security.Authentication.SslProtocols.Tls12
        //     //};
        //     //optionsBuilder.UseMongoDb(settings);

        //     MongoClient mongoClient = new MongoClient(settings);
        //     optionsBuilder.UseMongoDb(mongoClient);
        // }
    }
}