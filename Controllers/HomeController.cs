﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AspNetMvc.Models;
using AspNetMvc.Data;

namespace AspNetMvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _db;

        public HomeController(ApplicationDbContext db)
        {
            this._db = db;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> About()
        {
            ViewData["Message"] = "Your application description page.";

            var borky = new Dog
            {
                Breed = "Fluffer",
                Name = "Borky"
            };

            // await _db.Dogs.AddAsync(borky);
            // await _db.SaveChangesAsync();

            var newOwner = new Owner { Name = "Bob" };

            newOwner.Dogs.Add(borky);

            await _db.Owners.AddAsync(newOwner);
            await _db.SaveChangesAsync();

            // newOwner.Dogs.Add(borky);

            // await _db.SaveChangesAsync();

            return View(newOwner);
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
