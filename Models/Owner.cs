using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;

namespace AspNetMvc.Models
{
    public class Owner
    {
        [Key]
        public ObjectId Id { get; set; }

        public string Name { get; set; }

        public List<Dog> Dogs { get; set; } = new List<Dog>();
    }
}