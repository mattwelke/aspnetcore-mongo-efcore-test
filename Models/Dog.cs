using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;

namespace AspNetMvc.Models
{
    public class Dog
    {
        [Key]
        public ObjectId Id { get; set; }

        public string Breed { get; set; }

        public string Name { get; set; }

        //public Owner Owner { get; set; }
    }
}