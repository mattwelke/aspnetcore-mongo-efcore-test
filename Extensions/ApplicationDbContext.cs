using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using AspNetMvc.Data;

namespace AspNetMvc.Extensions
{
    public static class Extensions
    {
        public static IServiceCollection AddZooDbContext(this IServiceCollection serviceCollection)
        {
            return serviceCollection.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseMongoDb("mongodb://localhost");
            });
        }
    }
}